#include <stdio.h>
#include <stdlib.h>

int main(void){
	int map[33][33]={0}, player[33][33]={0}, unti[512], x=1, y=1, k=0;

//マップの外埋め
	for(int i=0; i<33; i++){
		map[i][0] = 2;
	}
	for(int i=0; i<33; i++){
		map[i][32] = 2;
	}
	for(int i=0; i<33; i++){
		map[0][i] = 2;
	}
	for(int i=0; i<33; i++){
		map[32][i] = 2;
	}

//壁のもとを作ったりする

	for(int i=0; i<33; i+=2){
		for(int j=0; j<33; j+=2){
			map[i][j] = 1;
		}
	}

//ランダムに壁を立てる
	srand(7);
	for(int i=0; i<512; i++){
		unti[i] = rand();
	}

	for(int i=2; i<33; i+=2){
		for(int j=2; j<33; j+=2){
			if(unti[k]%4 == 0){
				map[i+1][j] = 2;
			}else if(unti[k]%4 == 1){
				map[i][j+1] = 2;
			}else if(unti[k]%4 == 2){
				map[i-1][j] = 2;
			}else map[i][j-1] = 2;
			k++;
		}
	}

//詰み壁削除用だけどうんちなのであってもなくても詰むときは詰む

	for(int i=1; i<33; i+=2){
		for(int j=1; j<33; j+=2){
			if(map[i+1][j] == 1 && map[i][j+1] == 1 && map[i-1][j] == 1 && map[i][j-1] == 1){
				map[i+1][j] = 0;
				map[i][j+1] = 0;
			}
		}
	}


	player[x][y] = 3;//操作するやつの初期座標指定

//こっからゲームが始まる
	while(1){

		for(int i=32; i>=0; i--){ //描画
			for(int j=0; j<33; j++){
				if(player[i][j] == 3) printf("x");
				else{
					if(map[i][j] == 0) printf(" ");
					else if(map[i][j] == 1) printf("+");
					else if(map[i][j] == 2){
						if(i%2 == 0 || i==0 || i==32) printf("-");
						else printf("|");

					}
				}
			}
			printf("\n");
		}


		if(x==31 && y==31){ //ゴール判定
			printf("GOAL!\n");
			break;
		}

		printf("nextdirection?(1:left, 2:right, 3:up, 4:down)");//入力受付
		scanf("%d", &k);

		if(k == 1){
			if(y > 2 && map[x][y-1] == 0){
				player[x][y] = 0;
				y-=2;
			}
		}else if(k == 2){
			if(y < 30 && map[x][y+1] == 0){
				player[x][y] = 0;
				y+=2;
			}			
		}else if(k == 3){
			if(x < 30 && map[x+1][y] == 0){
				player[x][y] = 0;
				x+=2;
			}
		}else if(k == 4){
			if(x > 2 && map[x-1][y] == 0){
				player[x][y] = 0;
				x-=2;
			}
		}else printf("指定した値を入れろカス\n");

		player[x][y] = 3;

	}
	return 0;
}